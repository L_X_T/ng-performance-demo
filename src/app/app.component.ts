import { DOCUMENT, isPlatformBrowser, isPlatformServer } from '@angular/common';
import { Component, inject, PLATFORM_ID } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';

@Component({
  selector: 'app-flight-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  standalone: true,
  imports: [SidebarComponent, NavbarComponent, RouterOutlet]
})
export class AppComponent {
  private platform = inject(PLATFORM_ID);
  private document = inject(DOCUMENT);

  constructor() {
    if (isPlatformBrowser(this.platform)) {
      // Safe to use document, window, localStorage, etc.
      // console.warn('browser');
      // console.log(document);
    }

    if (isPlatformServer(this.platform)) {
      // Not smart to use document here, but we can inject it :-)
      // console.log('server');
      // console.log(this.document);
    }
  }
}
